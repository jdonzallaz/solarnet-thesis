# ========== Dataset ==========
dataset: "..."


# ========== Paths ==========

# Choose destination for the dataset file(s). Default to current dir.
destination: data/cls-bz-24h-17-20
# Base filename for the metadata files. If splits-separate-files is true, the split's name will be appended to the
# filename. E.g.: sdo-dataset-train.csv
filename: sdo-dataset.csv
# Original path where the full dataset is stored
dataset_path: /data2/


# ========== Date range ==========

# Interval of date to handle when creating the dataset. It will search images and labels between those dates.
# The selected data is further refined with `splits` parameter. Format supported by pandas.Timestamp.
start-date: 1996-04-21T00:00:00
end-date: 2020-12-31T23:59:59


# ========== Images ==========

# Whether to write the path as relative (from dataset root) or absolute (using dataset_path) in the csv file
relative_paths: true

# Choose whether files (images) are copied to the destination folder.
copy: true

# Choose channel among: 94, 131, 171, 193, 211, 304, 335, 1600, 1700, 4500.
channel: magnetogram

# A sample will be created every "sample-interval". This will often be the same as "flare-period",
# but can be customized for special cases. Format supported by pandas.Timedelta.
sample-interval: 24h

# The different times before the flare to choose images. Format supported by pandas.Timedelta.
time-steps:
  - 0

# The time range to search for an image if not found at the exact search time.
# This is because there is not an image for each minute, and some images are missing.
# A range of 15 minutes will search 15m before and after the original time.
search-image-period: 30m


# ========== Flares ==========

# In which time span the target (peak flux) will be selected. Format supported by pandas.Timedelta.
flare-period: 24h

# Choose type of targets between regression and classification
# With classification, the classes must be defined as a list of key (name of the class) - value (comparison) which are
# evaluated in the defined order to map the peak flux to the classes.
target: regression  # to define a regression
# target: # to define a classification (example)
#   classes:
#     - Quiet: "< 1e-6"
#     - ">=C": ">= 1e-6"


# ========== Splits ==========

# Set of splits. Accepts either years (4 digits) or months (3 first letters) to include in each split.
# If there is both month and years in splits, years take priority.
# `ignore` is a special name of split permitting to filter out some data.
splits:
  train:
    - jan
    - feb
    - mar
    - apr
    - may
    - jun
    - jul
    - aug
    - sep
    - oct
    - nov
  val:
    - dec
  test:
    - 2017
    - 2018
    - 2019
    - 2020

# Add a margin between the split to avoid having too similar images. Format supported by pandas.Timedelta.
margin-between-splits: 0

# Whether to put splits in separate files. Otherwise, a column in the csv will specify the split
splits-separate-files: true
