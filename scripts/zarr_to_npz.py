from pathlib import Path
import numpy as np
from pathlib import Path
from tqdm import tqdm
import zarr
from datetime import datetime, timedelta


def process(dataset_path: Path, destination: Path, channel: str, instrument: str, max_cache_size_bytes: int = 10_000_000_000):
    store = zarr.DirectoryStore(dataset_path)
    cache = zarr.LRUStoreCache(store, max_size=max_cache_size_bytes)
    root = zarr.open(store=cache, mode="r")

    years = [str(y) for y in range(2010, 2021)]

    for year in tqdm(years):
        data = root[year][channel]
        obs_times = data.attrs["T_OBS"]

        # Loop on all samples
        for i, sample in enumerate(tqdm(data)):
            # Get OBS time
            obs_time = obs_times[i]
            try:
                obs_time = datetime.strptime(obs_time, "%Y.%m.%d_%H:%M:%S_TAI")
            except:
                obs_time = obs_time[:-7]
                obs_time = datetime.strptime(obs_time, "%Y.%m.%d_%H:%M")
                obs_time += timedelta(minutes=1)

            # Create path
            str_datetime = obs_time.strftime("%Y%m%d_%H%M")
            year = str(obs_time.year)
            month = f"{obs_time.month:02d}"
            day = f"{obs_time.day:02d}"
            filepath = destination / channel / year / month / day / f"{instrument}{str_datetime}_{channel.lower()}.npz"

            # Save to path
            filepath.parent.mkdir(parents=True, exist_ok=True)
            np.savez_compressed(filepath, x=sample)


dataset_path = Path("/data2/sdomlv2_full/sdomlv2_hmi.zarr/")
destination = Path("/data2/sdomlv2/")
process(dataset_path, destination, "Bz", "HMI")
