from datetime import datetime, timedelta
from io import BytesIO
from pathlib import Path
import requests
from typing import Optional

from PIL import Image
import re
import pandas as pd
import time
from tqdm import tqdm
import traceback
import typer

WAVELENGTH_TO_SOURCEID = {
    "sdo": {
        "94": 8,
        "131": 9,
        "171": 10,
        "193": 11,
        "211": 12,
        "304": 13,
        "335": 14,
        "1600": 15,
        "1700": 16,
        "4500": 17,
        "continuum": 18,
        "magnetogram": 19,
    },
    "soho": {
        "171": 0,
        "195": 1,
        "284": 2,
        "304": 3,
        "continuum": 7,
        "magnetogram": 6,
    },
}
WAVELENGTH_TO_INSTRUMENT = {
    "sdo": {
        "94": "AIA",
        "131": "AIA",
        "171": "AIA",
        "193": "AIA",
        "211": "AIA",
        "304": "AIA",
        "335": "AIA",
        "1600": "AIA",
        "1700": "AIA",
        "4500": "AIA",
        "continuum": "HMI",
        "magnetogram": "HMI",
    },
    "soho": {
        "171": "EIT",
        "195": "EIT",
        "284": "EIT",
        "304": "EIT",
        "continuum": "MDI",
        "magnetogram": "MDI",
    },
}

# API_URL = "https://helioviewer-api.ias.u-psud.fr//v2/getJP2Image/?date={}&sourceId={}"
# API_URL = "https://api.helioviewer.org/v2/getJP2Image/?date={}&sourceId={}"
# API_URL = "http://dmlab.cs.gsu.edu/dmlabapi/images/SDO/AIA/{}/?wave={}&starttime={}"
BASE_API_URL = "https://api.helioviewer.org/v2"
BACKUP_API_URL = "https://helioviewer-api.ias.u-psud.fr//v2"
GET_JP2_IMAGE_URL = "{}/getJP2Image/?date={}&sourceId={}"
GET_CLOSEST_IMAGE_DATA_URL = "{}/getClosestImage/?date={}&sourceId={}"


def get_image(
    date: datetime, observatory: str = "sdo", wavelength: str = "magnetogram", save_path: Optional[Path] = None
):
    url = GET_JP2_IMAGE_URL.format(
        BASE_API_URL, date.isoformat() + "Z", WAVELENGTH_TO_SOURCEID[observatory][wavelength]
    )
    response = requests.get(url)
    # img = Image.open(BytesIO(response.content), formats=["JPEG2000"])

    with open(save_path, "wb") as f:
        f.write(response.content)

    # if save_path is not None:
    # save_path.parent.mkdir(parents=True, exist_ok=True)
    # img.save(save_path)
    # save_path_1024 = save_path.parent / (save_path.stem + "_1024" + save_path.suffix)
    # img.resize((1024, 1024), Image.LANCZOS).save(save_path_1024)

    # return img


def get_closest_image_data(date: datetime, observatory: str = "sdo", wavelength: str = "magnetogram"):
    url = GET_CLOSEST_IMAGE_DATA_URL.format(
        BASE_API_URL, date.isoformat() + "Z", WAVELENGTH_TO_SOURCEID[observatory][wavelength]
    )
    response = requests.get(url)

    return response.json()


def get_save_image(
    date: datetime,
    observatory: str = "sdo",
    wavelength: str = "magnetogram",
):
    date_filtered = re.sub("[-:]", "", date.isoformat())
    instrument = WAVELENGTH_TO_INSTRUMENT[observatory][wavelength]
    path_str = f"static/images/{instrument}_{wavelength}_{date_filtered}.jpg"
    path = Path(path_str)

    if path.exists() and path.is_file():
        img = Image.open(path)
    else:
        img = get_image(date, observatory=observatory, save_path=path)

    return img


def date_generator(start_date):
    while True:
        yield start_date
        start_date = start_date + timedelta(hours=1)


# python -m down-cli -d /data1/data/sdo-dataset-jp2-soho/ -c ./downloaded_data_soho.csv -s 1996-04-21T00:00:00 -e 2011-01-11T22:00:00 -o soho
def main(
    base_path: Path = typer.Option(..., "--data-path", "-d"),
    csv_path: Path = typer.Option(..., "--csv-path", "-c"),
    str_start_date: str = typer.Option(..., "--start", "-s"),
    str_end_date: str = typer.Option("2022-03-01T00:00:00", "--end", "-e"),
    observatory: str = typer.Option("sdo", "--obs", "-o"),
    wavelength: str = typer.Option("magnetogram", "--wavelength", "-w"),
):
    # str_start_date = "2010-12-06T07:00:00"
    # str_start_date = "2011-07-22T00:00:00"
    # str_start_date = "2013-06-25T10:00:00"
    start_date = datetime.strptime(str_start_date, "%Y-%m-%dT%H:%M:%S")
    # str_end_date = "2010-12-07T17:00:00"
    # str_end_date = "2022-03-01T00:00:00"
    end_date = datetime.strptime(str_end_date, "%Y-%m-%dT%H:%M:%S")
    # observatory = "sdo"
    # wavelength = "magnetogram"
    # base_path = Path("./test/") / "images"
    # base_path = Path("/data1/data/sdo-dataset-jp2/")
    base_path.mkdir(parents=True, exist_ok=True)

    # csv
    # csv_path = Path("./downloaded_data.csv")
    # csv_error_path = Path("./downloaded_data_error.csv")
    csv_error_path = csv_path.parent / (csv_path.stem + "_error" + csv_path.suffix)

    def save_to_csv(dicts, path):
        df = pd.DataFrame.from_dict(dicts)

        if path.exists():
            df.to_csv(path, mode="a", index=False, header=False)
        else:
            df.to_csv(path, index=False, header=True)

    data_dicts = []
    data_error_dicts = []
    with tqdm(date_generator(start_date)) as t:
        for date in t:
            if date > end_date:
                break

            t.set_postfix(date=date.isoformat())

            data_dict = {}
            try:
                img_data = get_closest_image_data(date, observatory=observatory, wavelength=wavelength)
                # print(img_data)

                date_filtered = re.sub("[-:]", "", date.isoformat())
                instrument = WAVELENGTH_TO_INSTRUMENT[observatory][wavelength]
                path = base_path / f"{instrument}_{wavelength}_{date_filtered}.jp2"

                if "error" in img_data:
                    img_data["date"] = date.isoformat()
                    data_error_dicts.append(img_data)
                    continue

                data_dict = {
                    "id": img_data["id"],
                    "date": img_data["date"],
                    "name": img_data["name"],
                    "scale": img_data["scale"],
                    "width": img_data["width"],
                    "height": img_data["height"],
                    "refPixelX": img_data["refPixelX"],
                    "refPixelY": img_data["refPixelY"],
                    "requested_date": date.isoformat(),
                    "path": str(path),
                    "observatory": observatory,
                    "instrument": instrument,
                    "wavelength": wavelength,
                }

                # st = time.perf_counter()
                get_image(date, observatory=observatory, wavelength=wavelength, save_path=path)
                # et = time.perf_counter()
                # print("time", et - st)
                data_dicts.append(data_dict)
            except Exception as e:
                print(f"An error occured while processing date: {date}")
                print(e)
                traceback.print_exception(type(e), e, e.__traceback__)
                break
            # break

    save_to_csv(data_dicts, csv_path)
    save_to_csv(data_error_dicts, csv_error_path)


if __name__ == "__main__":
    typer.run(main)
