# SolarNet SSL Bz model

## Description

Pre-trained model on solar image data (magnetogram). Trained on SDO-Dataset. Input is a 2D tensor / 1-channel image (SDO/HMI Bz). Output is a 2048-dim tensor represnetation of the image.
It can be finetuned on any downstream task by adding a classification head or any kind of network.

## Config

```yaml
model: SimCLR
output_size: 2
backbone: resnet50
backbone_output_size: 2048
channels: 1
```

## Shape

- Input shape: `torch.Size([1, 128, 128])`
- Output shape of backbone: `torch.Size([2048])`

## Author

Jonathan Donzallaz <[jonathan.donzallaz@hefr.ch](mailto:jonathan.donzallaz@hefr.ch)>
