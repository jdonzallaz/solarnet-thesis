# Model report
## Training
### Parameters
```yaml
data:
  name: sdo-benchmark
  validation_size: 0.1
  channel: magnetogram
  size: 256
  targets:
    classes:
    - Quiet: < 1e-6
    - '>=C': '>= 1e-6'
  time_steps: 3
  path: data/sdo-benchmark
model:
  backbone: simple-cnn
  learning_rate: 0.005
  activation: relu6
  pooling: 2
  dropout: 0.2
  n_hidden: 16
  lr_scheduler: true
trainer:
  epochs: 2
  patience: 12
  batch_size: 128
name: Baseline binary classification model, magnetogram, >=C, multi-channel(mag,211,193,94), max 40 epochs
training_type: train
tune_lr: false
path: models/baseline_binary_sdobenchmark
seed: 42
tracking: true
system:
  gpus: 1
  workers: 20
tags:
- sdo-benchmark
- Class-C
```
### Model architecture
```
ImageClassification(
  (backbone): Sequential(
    (0): SimpleCNN(
      (conv_blocks): Sequential(
        (conv_block_0): Sequential(
          (conv): Conv2d(4, 16, kernel_size=(3, 3), stride=(3, 3))
          (batchnorm): BatchNorm2d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
          (pooling): MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0, dilation=1, ceil_mode=False)
          (activation): ReLU6()
          (dropout): Dropout2d(p=0.2, inplace=False)
        )
        (conv_block_1): Sequential(
          (conv): Conv2d(16, 32, kernel_size=(3, 3), stride=(3, 3))
          (batchnorm): BatchNorm2d(32, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
          (pooling): MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0, dilation=1, ceil_mode=False)
          (activation): ReLU6()
          (dropout): Dropout2d(p=0.2, inplace=False)
        )
        (conv_block_2): Sequential(
          (conv): Conv2d(32, 64, kernel_size=(3, 3), stride=(3, 3))
          (batchnorm): BatchNorm2d(64, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
          (pooling): MaxPool2d(kernel_size=(2, 2), stride=(2, 2), padding=0, dilation=1, ceil_mode=False)
          (activation): ReLU6()
          (dropout): Dropout2d(p=0.2, inplace=False)
        )
      )
    )
    (1): AdaptiveAvgPool2d(output_size=(1, 1))
    (2): Flatten(start_dim=1, end_dim=-1)
  )
  (classifier): Classifier(
    (classifier): Sequential(
      (0): Flatten(start_dim=1, end_dim=-1)
      (1): Dropout(p=0.2, inplace=False)
      (2): Linear(in_features=64, out_features=16, bias=True)
      (3): BatchNorm1d(16, eps=1e-05, momentum=0.1, affine=True, track_running_stats=True)
      (4): ReLU(inplace=True)
      (5): Dropout(p=0.2, inplace=False)
      (6): Linear(in_features=16, out_features=2, bias=True)
    )
  )
  (loss_fn): CrossEntropyLoss()
  (train_accuracy): BinaryAccuracy()
  (val_accuracy): BinaryAccuracy()
  (test_metrics): MetricCollection(
    (BinaryAccuracy): BinaryAccuracy()
    (BinaryF1Score): BinaryF1Score()
    (BinaryRecall): BinaryRecall()
    (BinaryStatScores): BinaryStatScores()
  )
)
================================================================================
Total parameters: 25058
```
### Loss curve
![Loss curve](train_plots/loss_curve.png 'Loss curve')

### Accuracy curve
![Accuracy curve](train_plots/accuracy_curve.png 'Accuracy curve')

### Metadata
```yaml
machine: 'lambda02 | Linux #113-Ubuntu SMP Thu Jul 9 23:41:39 UTC 2020 | 10 cores @ 4120.00Mhz | RAM 126 GB | 2x TITAN RTX'
training_time: 16.31s
model_size: 333kB
early_stopping_epoch: 0
model_checkpoint_step: 58
model_checkpoint_epoch: 1
tracking_id: SOLN-460
data:
  class-balance:
    train:
      Quiet: 4332
      '>=C': 2979
    val:
      Quiet: 479
      '>=C': 333
    test:
      Quiet: 345
      '>=C': 510
  shape: (4, 256, 256)
  tensor-data:
    min: -1.0
    max: 1.0
    mean: -0.1405787616968155
    std: 0.32240188121795654
  set-sizes:
    train: 7311
    val: 812
    test: 855
```
## Test
### Metrics
| Path                                             | accuracy   | balanced_accuracy   | csi    | f1    | far    | hss    | pod    | tss    |
|--------------------------------------------------|------------|---------------------|--------|-------|--------|--------|--------|--------|
| models/baseline_binary_sdobenchmark/metrics.yaml | 0.8082     | 0.8255              | 0.7197 | 0.837 | 0.1512 | 0.6041 | 0.8255 | 0.6081 |

### Confusion matrix
![Confusion matrix](test_plots/confusion_matrix.png 'Confusion matrix')

### ROC Curve
![ROC Curve](test_plots/roc_curve.png 'ROC Curve')

### Test samples
![Test samples](test_plots/test_samples.png 'Test samples')

