from pathlib import Path
import logging
import math
import shutil

import pytorch_lightning as pl
import matplotlib.pyplot as plt
from torch.nn.functional import softmax
import torch
import numpy as np
from torchvision import transforms
from pytorch_lightning import seed_everything
from tqdm import tqdm
import pandas as pd
from PIL import Image, UnidentifiedImageError
import time
from multiprocessing import Pool, get_context

from solarnet.utils.plots import plot_confusion_matrix, plot_image_grid
from solarnet.utils.target import flux_to_class_builder
from solarnet.utils.log import init_log, set_log_level
from solarnet.models import ImageClassification
from solarnet.data import SDOBenchmarkDataModule, SDODatasetDataModule, SDODataset
from solarnet.utils.yaml import load_yaml
from solarnet.data import datamodule_from_config
from solarnet.data.transforms import sdo_dataset_normalize, SDOSimCLRDataTransform
from solarnet.utils.plots import plot_confusion_matrix
from solarnet.utils.metrics import classification_metrics

init_log()
set_log_level(logging.INFO)


def filter_sample_not_in_range(samples: pd.DataFrame, range: int, column1: str, column2: str) -> pd.DataFrame:
    df = samples.copy()
    df[column1] = pd.to_datetime(df[column1])
    df[column2] = pd.to_datetime(df[column2])
    df["time_delta"] = abs((df[column1] - df[column2]).dt.total_seconds())

    return samples[df.time_delta < range]


def create_circular_mask(h, w, center=None, radius=None):

    if center is None:  # use the middle of the image
        center = (int(w / 2), int(h / 2))
    if radius is None:  # use the smallest distance between the center and image walls
        radius = min(center[0], center[1], w - center[0], h - center[1])

    Y, X = np.ogrid[:h, :w]
    dist_from_center = np.sqrt((X - center[0]) ** 2 + (Y - center[1]) ** 2)

    mask = dist_from_center <= radius
    return mask


def preprocess_image(img: Image.Image, resize: int = 128, scale: float = 0.6):
    size = img.size[0]

    BASE_SIZE_SDO = 4096
    BASE_RADIUS_SDO = 1600  # px, of the 4096 px image
    BASE_SCALE_SDO = 0.6

    BASE_SIZE_SOHO = 1024
    BASE_RADIUS_SOHO = 968  # px, of the 1024 px image
    BASE_SCALE_SOHO = 1.97784

    # Remove black background around the sun in jpg images
    radius_factor = (BASE_RADIUS_SDO - 1) / BASE_SIZE_SDO  # -1, to remove the thin black margin

    mask = create_circular_mask(size, size, center=(size // 2, size // 2), radius=size * radius_factor)

    def remove_background(x):
        x_copy = x.clone()
        if len(x.shape) == 3:
            x_copy[0][~mask] = 0.5
        else:
            x_copy[~mask] = 0.5
        return x_copy

    # Downsizing by averaging in local blocks
    target_mean_size = max(512, resize)
    factor = size // target_mean_size
    mean_downsize = lambda x: torch.nn.functional.avg_pool2d(x, factor)

    if scale < 1:  # SDO
        scale_factor = scale / BASE_SCALE_SDO
        translate = [-0.5, 1]
    else:
        scale_soho_to_sdo_factor = (BASE_RADIUS_SDO / BASE_SIZE_SDO) / (BASE_RADIUS_SOHO / BASE_SIZE_SOHO) * 2
        scale_factor = scale / BASE_SCALE_SOHO * scale_soho_to_sdo_factor
        translate = [0.5, 0.5]

    transform = transforms.Compose(
        [
            transforms.ToTensor(),
            transforms.Lambda(
                lambda x: transforms.functional.affine(x, angle=0, translate=translate, scale=scale_factor, shear=0)
            ),
            transforms.Lambda(remove_background),
            transforms.Lambda(mean_downsize),
            # transforms.CenterCrop((target_mean_size // 2, target_mean_size - target_mean_size // 8)),
            # transforms.Resize(resize),
            transforms.Normalize(mean=[0.5], std=[0.5]),
        ]
    )
    return transform(img)


def process_sample(sample):
    path = Path(sample["path"])
    obs = sample["observatory"].upper()
    inst = sample["instrument"].upper()
    wl = sample["wavelength"]
    scale = sample["scale"]
    date = sample["date"]
    date = date.round("min")

    year = str(date.year)
    month = f"{date.month:02d}"
    day = f"{date.day:02d}"
    hours = f"{date.hour:02d}"
    minutes = f"{date.minute:02d}"

    new_path = dest_path / f"{obs}_{inst}_{wl}_{year}{month}{day}_{hours}{minutes}.pt"

    if new_path.exists():
        return

    # print(f"time: {time.perf_counter()-t1:.2f}s")
    # t1 = time.perf_counter()
    try:
        image = Image.open(path, formats=["JPEG2000"])
    except UnidentifiedImageError as e:
        print(f"UnidentifiedImageError: {path}")
        return
    # print(f"time: {time.perf_counter()-t1:.2f}s")
    # t1 = time.perf_counter()
    try:
        tensor = preprocess_image(image, resize=512, scale=scale)
    except OSError as e:
        print(f"OSError: {path}")
        return
    # print(f"time: {time.perf_counter()-t1:.2f}s")
    # t1 = time.perf_counter()
    torch.save(tensor, new_path)
    # print(f"time: {time.perf_counter()-t1:.2f}s")

    # shutil.copy2(path, new_path)


if __name__ == "__main__":
    df = pd.read_csv(
        "../downloaded_data_soho.csv",
        # names=[
        #     "id",
        #     "date",
        #     "name",
        #     "scale",
        #     "width",
        #     "height",
        #     "refPixelX",
        #     "refPixelY",
        #     "requested_date",
        #     "path",
        #     "observatory",
        #     "instrument",
        #     "wavelength",
        # ],
        parse_dates=["date", "requested_date"],
    )
    df = filter_sample_not_in_range(df, 60 * 30, "date", "requested_date")

    records = df.to_dict("records")

    dest_path = Path("/data1/data/sdo-dataset-jp2-cleaned/")
    dest_path.mkdir(parents=True, exist_ok=True)

    t1 = time.perf_counter()
    # p = Pool(4)
    with Pool(20) as pool:
        # with get_context("fork").Pool(10) as pool:
        # pool.map(process_sample, records[:20])
        list(tqdm(pool.imap_unordered(process_sample, records), total=len(records)))
    # for sample in tqdm(records[:20]): # 5:40
    # process_sample(sample)
    # print(sample)
    # t1 = time.perf_counter()

    # break
    print(f"time: {time.perf_counter()-t1:.2f}s")

# UnidentifiedImageError: /data1/data/sdo-dataset-jp2/HMI_magnetogram_20110721T230000.jp2
# OSError: /data1/data/sdo-dataset-jp2/HMI_magnetogram_20170910T030000.jp2
# OSError: /data1/data/sdo-dataset-jp2/HMI_magnetogram_20170910T060000.jp2
# OSError: /data1/data/sdo-dataset-jp2/HMI_magnetogram_20170910T160000.jp2
# UnidentifiedImageError: /data1/data/sdo-dataset-jp2-soho/MDI_magnetogram_19970627T110000.jp2
