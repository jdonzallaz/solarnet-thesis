from typing import Union
from pl_bolts.models.self_supervised import BYOL as BYOL_bolts
from torch import nn

from solarnet.models.model_utils import BaseModel


class BYOL(BYOL_bolts, BaseModel):
    """
    This class is wrapper around BYOL class from lightning-bolts.
    It adds a single parameter "n_channel" and overrides the first layer of the resnet encoder with the given number
      of channel (default: 3).
    The dataset parameter is now optional (default to ""). Other parameters (gpus, num_samples, batch_size) receive meaningful default.
    The arch parameter is renamed to "backbone".
    The "encoder" module is renamed to "backbone".
    """

    def __init__(
        self,
        learning_rate: float = 0.2,
        weight_decay: float = 1.5e-6,
        warmup_epochs: int = 10,
        max_epochs: int = 1000,
        encoder_out_dim: int = 2048,
        projector_hidden_dim: int = 4096,
        projector_out_dim: int = 256,
        initial_tau: float = 0.996,
        backbone: str = "resnet50",
        n_channel: int = 3,
    ):
        super().__init__(
            learning_rate=learning_rate,
            weight_decay=weight_decay,
            warmup_epochs=warmup_epochs,
            max_epochs=max_epochs,
            base_encoder=backbone,
            encoder_out_dim=encoder_out_dim,
            projector_hidden_dim=projector_hidden_dim,
            projector_out_dim=projector_out_dim,
            initial_tau=initial_tau,
            n_channel=n_channel,
            backbone=backbone,  # Doubled here so it's added to hparams
        )

        self.n_channel = n_channel

        if self.n_channel != 3:
            # Other values are the default found in resnet models.
            self.online_network.encoder.conv1 = nn.Conv2d(
                self.n_channel,
                64,
                kernel_size=(7, 7),
                stride=(2, 2),
                padding=(3, 3),
                bias=False,
            )
            self.target_network.encoder.conv1 = nn.Conv2d(
                self.n_channel,
                64,
                kernel_size=(7, 7),
                stride=(2, 2),
                padding=(3, 3),
                bias=False,
            )

        self.backbone = backbone
        self.encoder_out_dim = encoder_out_dim

    @property
    def backbone_name(self) -> str:
        return self.backbone

    @property
    def output_size(self) -> int:
        return self.encoder_out_dim
