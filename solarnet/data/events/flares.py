import logging
from pathlib import Path
from typing import Optional

import pandas as pd
from sunpy.net import Fido, attrs as a

from solarnet.utils.physics import class_to_flux

logger = logging.getLogger(__name__)


def find_flare_peak_flux(
    df: pd.DataFrame,
    start: pd.Timestamp,
    end: pd.Timestamp,
    column_goes_cls: str = "fl_goescls",
) -> float:
    """
    Find the maximum peak_flux in an interval [start:end]. It searches in a dataframe which must be
    indexed by date.
    """

    flares_df = df.loc[start:end]
    flux_values = flares_df[column_goes_cls].values

    if len(flux_values) == 0:
        return 1e-9

    return max(map(class_to_flux, flux_values))


def get_flares_cache_filename(datetime_start: pd.Timestamp, datetime_end: pd.Timestamp):
    """
    Return a correctly formatted filename for a parquet flares cache file, with dates (range) for reference.
    """

    return (
        f"hek_flares_{datetime_start.isoformat().replace(':', '-')}_"
        f"{datetime_end.isoformat().replace(':', '-')}.parquet"
    )


def get_flares_from_cache(datetime_start: pd.Timestamp, datetime_end: pd.Timestamp) -> Optional[pd.DataFrame]:
    """
    Check in the cache for data in the given dates range. Return None if nothing is found.
    """

    folder = Path.home() / ".solarnet" / "hek"
    filename = get_flares_cache_filename(datetime_start, datetime_end)
    path = folder / filename

    if not path.exists():
        return None

    logger.info("Loading flares from cache")

    try:
        df = pd.read_parquet(path)
    except Exception:
        logger.warning("Error while loading flares cache")
        return None

    return df


def write_flares_to_cache(df: pd.DataFrame, datetime_start: pd.Timestamp, datetime_end: pd.Timestamp):
    """
    Write the given dataframe to a .parquet file for caching. Uses the given dates range for reference.
    The cache is in the .solarnet/hek/ folder of the user home directory.
    """

    folder = Path.home() / ".solarnet" / "hek"
    folder.mkdir(parents=True, exist_ok=True)
    filename = get_flares_cache_filename(datetime_start, datetime_end)
    path = folder / filename

    logger.info("Writing flares to cache")

    try:
        df.to_parquet(path)
    except Exception:
        logger.warning("Error while writing flares to cache")


def get_flares(datetime_start: pd.Timestamp, datetime_end: pd.Timestamp):
    df = get_flares_from_cache(datetime_start, datetime_end)
    if df is not None:
        return df

    observatory = "GOES"
    instrument = "GOES"
    from_name = "SWPC"
    columns = [
        "frm_name",
        "obs_observatory",
        "obs_instrument",
        "ar_noaanum",
        "event_starttime",
        "event_endtime",
        "event_peaktime",
        "fl_goescls",
        "fl_peakflux",
    ]

    result = Fido.search(
        a.Time(datetime_start, datetime_end),
        a.hek.FL,
        a.hek.OBS.Observatory == observatory,
        a.hek.OBS.Instrument == instrument,
        a.hek.FRM.Name == from_name,
    )

    hek = result["hek"]
    hek.keep_columns(columns)
    hek = hek[hek["ar_noaanum"] != 0]
    hek = hek[hek["fl_goescls"] != ""]
    df = hek.to_pandas()

    # Index by event_peaktime column and parse date
    df = df.astype({"event_peaktime": "datetime64[ns]"})
    df = df.set_index("event_peaktime")
    df = df.sort_index()

    write_flares_to_cache(df, datetime_start, datetime_end)

    return df
