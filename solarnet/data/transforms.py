import math
from typing import Callable, List, Optional, Tuple, Union

import torch
from torchvision.transforms import transforms, InterpolationMode
import torchvision.transforms.functional as transforms_functional

# Same preprocess as github.com/i4Ds/SDOBenchmark
CHANNEL_PREPROCESS = {
    "94": {"min": 0.1, "max": 800, "scaling": "log10"},
    "131": {"min": 0.7, "max": 1900, "scaling": "log10"},
    "171": {"min": 5, "max": 3500, "scaling": "log10"},
    "193": {"min": 20, "max": 5500, "scaling": "log10"},
    "211": {"min": 7, "max": 3500, "scaling": "log10"},
    "304": {"min": 0.1, "max": 3500, "scaling": "log10"},
    "335": {"min": 0.4, "max": 1000, "scaling": "log10"},
    "1600": {"min": 10, "max": 800, "scaling": "log10"},
    "1700": {"min": 220, "max": 5000, "scaling": "log10"},
    "4500": {"min": 4000, "max": 20000, "scaling": "log10"},
    "continuum": {"min": 0, "max": 65535, "scaling": None},
    "magnetogram": {"min": -250, "max": 250, "scaling": None},
    "bx": {"min": -250, "max": 250, "scaling": None},
    "by": {"min": -250, "max": 250, "scaling": None},
    "bz": {"min": -250, "max": 250, "scaling": None},
}


def sdo_dataset_normalize(channel: Union[str, int], resize: Optional[int] = None):
    """
    Apply the normalization necessary for the sdo-dataset. Depending on the channel, it:
      - flip the image vertically
      - clip the "pixels" data in the predefined range (see above)
      - apply a log10() on the data
      - normalize the data to the [0, 1] range
      - normalize the data around 0 (standard scaling)

    :param channel: The kind of data to preprocess
    :param resize: Optional size of image (integer) to resize the image
    :return: a transforms object to preprocess tensors
    """

    preprocess_config = CHANNEL_PREPROCESS[str(channel).lower()]

    lambda_transform = lambda x: torch.clamp(
        transforms_functional.vflip(x),
        min=preprocess_config["min"],
        max=preprocess_config["max"],
    )

    mean = preprocess_config["min"]
    std = preprocess_config["max"] - preprocess_config["min"]

    if preprocess_config["scaling"] == "log10":
        base_lambda = lambda_transform
        lambda_transform = lambda x: torch.log10(base_lambda(x))
        mean = math.log10(preprocess_config["min"])
        std = math.log10(preprocess_config["max"]) - math.log10(preprocess_config["min"])

    transform = [
        transforms.Lambda(lambda_transform),
        transforms.Normalize(mean=[mean], std=[std]),
        transforms.Normalize(mean=[0.5], std=[0.5]),
    ]

    if resize is not None:
        transform.insert(0, transforms.Resize(resize))

    return transforms.Compose(transform)


class SDOSimCLRDataTransform:
    """
    Prepare data for self-supervised training using SimCLR.
    This applies augmentations relevant for SDO image data (no color jittering or grayscale):
      - [WIP] random rotation
      - random resized crop
      - random horizontal flip
      - random vertical flip
    When this transform is called, it outputs 2 augmented tensors/image.
    Optionally, it can output a third transformed image with no augmentation (only resized) for online training.

    :param resize: The final image size after cropping
    :param do_online_transform: whether to output a third image for online training
    :param transform_before: optional transform(s) to apply before the augmentation transforms
    :param transform_after: optional transform(s) to apply after the augmentation transforms
    :param prob_random_horizontal_flip: probability for the random horizontal flip
    :param prob_random_vertical_flip: probability for the random vertical flip
    :param min_scale_resize: minimum scale of the cropping (0.5 means half the height of the original image)
    :param max_scale_resize: maximum scale of the cropping
    """

    def __init__(
        self,
        resize: int = 64,
        do_online_transform: bool = False,
        transform_before: Optional[Callable] = None,
        transform_after: Optional[Callable] = None,
        prob_random_horizontal_flip: float = 0.5,
        prob_random_vertical_flip: float = 0.5,
        min_scale_resize: float = 0.2,
        max_scale_resize: float = 0.8,
        # degree_random_rotation: float = 90,
        # prob_random_rotation: float = 0.5,
        interpolation: InterpolationMode = InterpolationMode.BILINEAR,
    ) -> None:

        self.resize = resize
        self.do_online_transform = do_online_transform
        self.transform_before = transform_before
        self.transform_after = transform_after
        self.prob_random_horizontal_flip = prob_random_horizontal_flip
        self.prob_random_vertical_flip = prob_random_vertical_flip
        self.min_scale_resize = min_scale_resize
        self.max_scale_resize = max_scale_resize
        # self.degree_random_rotation = degree_random_rotation
        # self.prob_random_rotation = prob_random_rotation
        self.interpolation = interpolation

        _transforms = [
            # transforms.RandomApply([transforms.RandomRotation(self.degree_random_rotation)], self.prob_random_rotation),
            # transforms.RandomResizedCrop(self.resize, scale=(min_scale_resize, max_scale_resize)),
            # ControlledRandomResizedCrop(self.resize, scale=(min_scale_resize, max_scale_resize)),
            transforms.RandomHorizontalFlip(prob_random_horizontal_flip),
            transforms.RandomVerticalFlip(prob_random_vertical_flip),
        ]
        _online_transforms = [transforms.Resize(self.resize, interpolation=self.interpolation)]

        if transform_before is not None:
            # _transforms.insert(0, transform_before)
            _online_transforms.insert(0, transform_before)
        if transform_after is not None:
            _transforms.append(transform_after)
            _online_transforms.append(transform_after)

        self.train_transform = transforms.Compose(_transforms)

        self.online_transform = transforms.Compose(_online_transforms)

    def __call__(self, sample):
        crrc = ControlledRandomResizedCrop(
            self.resize,
            scale=(self.min_scale_resize, self.max_scale_resize),
            interpolation=self.interpolation,
        )

        xi = self.transform_before(sample)
        xi = crrc(xi)
        xi = self.train_transform(xi)

        xj = self.transform_before(sample)
        xj = crrc(xj)
        xj = self.train_transform(xj)

        if self.do_online_transform:
            return xi, xj, self.online_transform(sample)
        return xi, xj


class ControlledRandomResizedCrop(transforms.RandomResizedCrop):
    def __init__(self, *args, **kwargs):
        super(ControlledRandomResizedCrop, self).__init__(*args, **kwargs)
        self._patch: Tuple[int, int, int, int] = None  # i, j, h, w
        self._overlap: float = 1 / 4

    @property
    def patch(self) -> Tuple[int, int, int, int]:
        return self._patch

    @patch.setter
    def patch(self, patch: Tuple[int, int, int, int]):
        self._patch = patch

    def forward(self, img):
        """
        Args:
            img (PIL Image or Tensor): Image to be cropped and resized.

        Returns:
            PIL Image or Tensor: Randomly cropped and resized image.
        """

        if self.patch is None:
            # Compute new patch without limitation
            patch = self.get_params(img, self.scale, self.ratio)
        else:
            # Compute new patch from current
            patch = self.controller_get_params(img, self.scale, self.ratio)

        # Save new patch
        self.patch = patch

        # Return resized crop
        return transforms_functional.resized_crop(img, *patch, self.size, self.interpolation, antialias=self.antialias)

    def controller_get_params(
        self, img: torch.Tensor, scale: List[float], ratio: List[float]
    ) -> Tuple[int, int, int, int]:
        """Get parameters for ``crop`` for a random sized crop, with overlap on self.patch.

        Args:
            img (PIL Image or Tensor): Input image.
            scale (list): range of scale of the origin size cropped
            ratio (list): range of aspect ratio of the origin aspect ratio cropped

        Returns:
            tuple: params (i, j, h, w) to be passed to ``crop`` for a random
            sized crop.
        """

        _, height, width = transforms_functional.get_dimensions(img)
        area = height * width

        log_ratio = torch.log(torch.tensor(ratio))
        for _ in range(10):
            target_area = area * torch.empty(1).uniform_(scale[0], scale[1]).item()
            aspect_ratio = torch.exp(torch.empty(1).uniform_(log_ratio[0], log_ratio[1])).item()

            w = int(round(math.sqrt(target_area * aspect_ratio)))
            h = int(round(math.sqrt(target_area / aspect_ratio)))

            if 0 < w <= width and 0 < h <= height:
                break
        else:
            # Fallback to central crop
            in_ratio = float(width) / float(height)
            if in_ratio < min(ratio):
                w = width
                h = int(round(w / min(ratio)))
            elif in_ratio > max(ratio):
                h = height
                w = int(round(h * max(ratio)))
            else:  # whole image
                w = width
                h = height

        pi, pj, ph, pw = self.patch

        min_overlap_w = int(max(pw * self._overlap, w * self._overlap))
        min_j = max(0, pj + min_overlap_w - w)
        max_j = min(width - w, pj + pw - min_overlap_w)

        min_overlap_h = int(max(ph * self._overlap, h * self._overlap))
        min_i = max(0, pi + min_overlap_h - h)
        max_i = min(height - h, pi + ph - min_overlap_h)

        i = torch.randint(min_i, max_i + 1, size=(1,)).item()
        j = torch.randint(min_j, max_j + 1, size=(1,)).item()

        return i, j, h, w
