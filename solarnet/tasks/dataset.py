import logging
from re import U
import shutil
from pathlib import Path
from time import strptime
from typing import Dict, Iterable, List, Optional, Tuple, Union

import pandas as pd
from tqdm import tqdm
from solarnet.data.events.flares import find_flare_peak_flux, get_flares

from solarnet.utils.dict import print_dict
from solarnet.utils.target import flux_to_class_builder
from solarnet.utils.yaml import write_yaml

logger = logging.getLogger(__name__)

# TODO: overrides parameters in CLI


def make_dataset(parameters: dict):
    print_dict(parameters)

    dataset = parameters.get("dataset", "sdo-dataset")

    # Prepare and verify paths
    destination = Path(parameters["destination"])
    destination.mkdir(parents=True, exist_ok=True)
    csv_path = destination / parameters["filename"]
    dataset_path = Path(parameters["dataset_path"])
    relative_paths = parameters["relative_paths"]
    if not dataset_path.exists():
        raise ValueError("Dataset not found at this location")

    # Prepare time / ranges parameters
    time_steps = [pd.Timedelta(t) for t in parameters["time-steps"]]
    flare_period = pd.Timedelta(parameters["flare-period"])
    sample_interval = pd.Timedelta(parameters["sample-interval"])
    search_image_period = pd.Timedelta(parameters["search-image-period"])
    datetime_start = pd.Timestamp(parameters.get("start-date", "2010-05-13T00:00:00"))
    datetime_end = pd.Timestamp(parameters.get("end-date", "2018-12-31T23:59:59"))

    # Splits
    splits = parameters["splits"]
    margin_between_splits = pd.Timedelta(parameters["margin-between-splits"])
    splits_separate_files = parameters["splits-separate-files"]
    if splits is None or len(splits) < 1:
        raise AttributeError("At least one split is required")
    split_dict = {
        strptime(part, "%b").tm_mon if isinstance(part, str) else part: split
        for split, parts in splits.items()
        for part in parts
    }

    # Prepare other parameters
    copy = parameters["copy"]
    channel = parameters["channel"]
    target = parameters["target"]
    classification = "classes" in target

    # Write config
    write_yaml(destination / "sdo-dataset-config.yaml", parameters)

    # Init dates
    time_offset = pd.DateOffset(seconds=sample_interval.total_seconds())
    intervals = pd.interval_range(datetime_start, datetime_end, freq=time_offset)

    # Load flares data
    logger.info("Downloading flares from HEK...")
    flares = get_flares(datetime_start, datetime_end)
    logger.info(flares.head())
    # logger.info(flares.to_dict("records")[0])
    # logger.info(flares.to_dict("records")[5])
    # logger.info(flares.to_dict("records")[-5])
    # logger.info(flares.to_dict("records")[-1])
    # return

    samples: List[Dict[str, Union[List[str], float, pd.Timestamp, bool]]] = []
    old_split = None
    current_split_date = None
    old_split_date = None
    logger.info("Searching corresponding image files in the dataset...")
    for interval in tqdm(intervals, desc="Date intervals", disable=logging.root.level > logging.INFO):
        sample_datetime: pd.Timestamp = interval.left

        # Find image paths in dataset folder / verify if images exist
        try:
            images_paths, all_found = find_paths(
                dataset, dataset_path, interval.left, time_steps, channel, search_image_period, relative_paths
            )
        except FileNotFoundError as e:
            continue

        # Find flares peak flux in this interval
        peak_flux = find_flare_peak_flux(flares, interval.left, interval.left + flare_period)

        # Prepare the split: choose month or year + ignore samples in the "margin" between splits
        current_split = split_dict.get(
            sample_datetime.year,
            split_dict.get(sample_datetime.month, None),
        )

        # Check split / ignore samples if not in split / ignore sample in margin between splits
        if current_split != old_split:
            old_split_date = current_split_date
        old_split = current_split
        current_split_date = sample_datetime
        if old_split_date is not None and current_split_date - margin_between_splits < old_split_date:
            continue
        if current_split is None:
            continue

        samples.append(
            {
                "images_paths": images_paths,
                "peak_flux": peak_flux,
                "datetime": sample_datetime,
                "all_found": all_found,
                "split": current_split,
            }
        )

    # Create dataframe of samples
    samples_ls = [[*i["images_paths"], i["peak_flux"], i["datetime"], i["all_found"], i["split"]] for i in samples]
    columns = [f"path_{i}_before" for i in parameters["time-steps"]] + ["peak_flux", "datetime", "all_found", "split"]
    df = pd.DataFrame(samples_ls, columns=columns)

    # Map target to classes if necessary
    if classification:
        target_transform = flux_to_class_builder(target["classes"], return_names=True)
        df["peak_flux"] = df["peak_flux"].map(target_transform)

    # Save samples list to csv
    if splits_separate_files:
        for current_split in splits:
            df_split = df[df["split"] == current_split]
            split_csv_path = csv_path.parent / f"{csv_path.stem}-{current_split}{csv_path.suffix}"
            df_split.to_csv(split_csv_path, index=False)
    else:
        df.to_csv(csv_path, index=False)

    # Copy data files to new folder
    if copy:
        paths = (
            path for sample in samples for path in sample["images_paths"] if sample["split"] != "ignore"
        )  # Generator of paths
        n_files = len(samples) * len(time_steps)
        copy_data(paths, dataset_path, destination, n_files)


def copy_data(paths: Iterable[str], dataset_path: Path, destination: Path, n_files: int):
    logger.info("Copy files to destination")
    count = 0
    for i in tqdm(paths, disable=logging.root.level > logging.INFO, total=n_files):
        from_ = dataset_path / i
        to_ = destination / i
        to_.parent.mkdir(parents=True, exist_ok=True)
        shutil.copy2(from_, to_)
        count += 1
    logger.info(f"Copied {count} files to {destination}")


def time_ranges_generator(datetime: pd.Timestamp, range: int = 1, unit: str = "min"):
    """
    Generate times around a datetime with a given range.
    It takes the initial datetime and generate:
        datetime - {range}{unit}, datetime + {range}{unit}, datetime - 2*{range}{unit}, datetime + 2*{range}{unit}, ...

    :param datetime: The initial datetime
    :param range: The range for the jumps
    :param unit: The unit of the range
    :return: yield an infinite number of datetime with given range
    """

    current_range = range
    while True:
        td = pd.Timedelta(current_range, unit)
        yield datetime - td
        yield datetime + td
        current_range += range


def get_obs_info(dataset: str, channel: Union[str, int], time: pd.Timestamp) -> Tuple[str, str]:
    if dataset == "sdo-dataset":
        if isinstance(channel, str) and channel.lower() in ["bx", "by", "bz"]:
            return ("SDO", "HMI")
        else:
            return ("SDO", "AIA")

    elif dataset == "helioviewer":
        if time < pd.Timestamp.fromisoformat("2010-12-06T07:00:00"):  # SOHO
            if isinstance(channel, str) and channel.lower() in ["magnetogram", "continuum"]:
                return ("SOHO", "MDI")
            else:
                return ("SOHO", "EIT")
        else:  # SDO
            if isinstance(channel, str) and channel.lower() in ["magnetogram", "continuum"]:
                return ("SDO", "HMI")
            else:
                return ("SDO", "AIA")

    else:
        raise ValueError("Dataset not recognized")


def make_path(dataset: str, base_path: Path, time: pd.Timestamp, channel: Union[str, int]) -> Path:
    """
    For `sdo-dataset` dataset, create path like:
    base/bz/2010/05/01/HMI20100501_1736_bz.npz
    base/171/2018/01/15/AIA20180114_0242_0171.npz

    For `helioviewer` dataset, create path like:
    base/SDO_HMI_magnetogram_20110601_0357.pt
    """

    str_datetime = time.strftime("%Y%m%d_%H%M")
    obs, instrument = get_obs_info(dataset, channel, time)

    filename_channel = channel
    if isinstance(filename_channel, int):
        filename_channel = f"{filename_channel:04d}"
    filename_channel = filename_channel.lower()

    if dataset == "sdo-dataset":
        year = str(time.year)
        month = f"{time.month:02d}"
        day = f"{time.day:02d}"
        return base_path / str(channel) / year / month / day / f"{instrument}{str_datetime}_{filename_channel}.npz"
    elif dataset == "helioviewer":
        return base_path / f"{obs}_{instrument}_{filename_channel}_{str_datetime}.pt"
    else:
        raise ValueError("Dataset not recognized")


def make_path_mix(base_path: Path, time: pd.Timestamp, channel: Union[str, int]) -> Path:
    """
    For `sdo-dataset` dataset, create path like:
    base/bz/2010/05/01/HMI20100501_1736_bz.npz
    base/171/2018/01/15/AIA20180114_0242_0171.npz

    For `helioviewer` dataset, create path like:
    base/SDO_HMI_magnetogram_20110601_0357.pt
    """

    if time < pd.Timestamp.fromisoformat("2010-05-01T00:00:00"):
        # hv
        dataset = "helioviewer"
        if isinstance(channel, str) and channel.lower() in ["bx", "by", "bz"]:
            channel = "magnetogram"
    else:
        # sdo-dataset
        dataset = "sdo-dataset"
        if isinstance(channel, str) and channel.lower() in ["magnetogram", "continuum"]:
            channel = "Bz"

    str_datetime = time.strftime("%Y%m%d_%H%M")
    obs, instrument = get_obs_info(dataset, channel, time)

    filename_channel = channel
    if isinstance(filename_channel, int):
        filename_channel = f"{filename_channel:04d}"
    filename_channel = filename_channel.lower()

    if dataset == "sdo-dataset":
        year = str(time.year)
        month = f"{time.month:02d}"
        day = f"{time.day:02d}"
        # TODO: fix
        # return base_path / "sdo-dataset" / str(channel) / year / month / day / f"{instrument}{str_datetime}_{filename_channel}.npz"
        return base_path / "sdomlv2" / str(channel) / year / month / day / f"{instrument}{str_datetime}_{filename_channel}.npz"
    elif dataset == "helioviewer":
        # return base_path / "sdo-dataset-jp2-cleaned" / f"{obs}_{instrument}_{filename_channel}_{str_datetime}.pt"
        return base_path / "jp2" / f"{obs}_{instrument}_{filename_channel}_{str_datetime}.pt"
    else:
        raise ValueError("Dataset not recognized")


def find_paths(
    dataset: str,
    base_path: Path,
    datetime: pd.Timestamp,
    time_steps: List[pd.Timedelta],
    channel: str,
    search_period: pd.Timedelta,
    relative_paths: bool,
) -> Tuple[List[Path], bool]:
    paths = []
    all_found = True

    for time_step in time_steps:
        datetime_before = datetime - time_step
        datetime_before = datetime_before.round("1min")  # TODO: test if still working with sdo-dataset + slower ?

        # path = make_path(dataset, base_path, datetime_before, channel)
        path = make_path_mix(base_path, datetime_before, channel)

        if not path.exists():
            all_found = False

            datetime_bottom_search_range = datetime_before - search_period
            datetime_top_search_range = datetime_before + search_period
            search_range = pd.Interval(datetime_bottom_search_range, datetime_top_search_range)

            gen = time_ranges_generator(
                datetime_before, range=1, unit="min"
            )  # TODO: test if still working with sdo-dataset + slower ?
            while True:
                new_time = next(gen)
                if new_time not in search_range:
                    raise FileNotFoundError()
                # path = make_path(dataset, base_path, new_time, channel)
                path = make_path_mix(base_path, new_time, channel)
                if path.exists():
                    break

        if relative_paths:
            path = path.relative_to(base_path)

        paths.append(path)

    return paths, all_found
